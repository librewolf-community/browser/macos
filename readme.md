# LibreWolf macOS

[Issue tracker](https://gitlab.com/librewolf-community/browser/macos/-/issues) for the macOS version of LibreWolf. Please note that if you ignore the pre-requisites and the template issues might be closed.

The browser is currently cross-compiled on Linux, check out the [bsys6](https://gitlab.com/librewolf-community/browser/bsys6) repository for builds and releases. The releases found in this repo are old ones, starting with version `115.0.2` we will release exclusively on `bsys6` and the `brew` cask will also point to that repo.

## Useful links

- [Website](https://librewolf.net/): read some docs and discover the project.
- [FAQ](https://librewolf.net/docs/faq/): most of your issues might already be solved there.
- [Homebrew](https://formulae.brew.sh/cask/librewolf#default): install using `brew install --cask librewolf`-
- [Releases](https://gitlab.com/librewolf-community/browser/bsys6/-/releases): for both x86 and arm64, although the second one is not tested and it could require [troubleshooting](https://gitlab.com/librewolf-community/browser/macos/-/issues/19).
- [Source](https://gitlab.com/librewolf-community/browser/source).
- [Settings](https://gitlab.com/librewolf-community/settings).
- Join us on [Matrix](https://matrix.to/#/#librewolf:matrix.org) / [Lemmy](https://lemmy.ml/c/LibreWolf/) / [Reddit](https://www.reddit.com/r/LibreWolf/).
